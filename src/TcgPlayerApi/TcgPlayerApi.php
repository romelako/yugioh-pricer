<?php

namespace TcgPlayerApi;

use Yugioh\Card;

class TcgPlayerApi
{
    const API_BASE = "http://partner.tcgplayer.com/x3/ygophl.asmx/p?";
    const PARTNER_KEY = "YugiohPricer";

    private $memcache;

    public function __construct()
    {
        $this->memcache = new \Memcache;
        $this->memcache->connect('localhost', 11211);
    }

    public function getCardPrices(Card $card)
    {
        if ($this->memcache->get($card->getMemcachekey())) {
            return $this->memcache->get($card->getMemcacheKey());
        } else {
            $query = http_build_query([
                "s"  => $card->getSet(),
                "p"  => $card->getName(),
                "n"  => $card->getTag(),
                "pk" => self::PARTNER_KEY
            ]);
            $url = self::API_BASE.$query;

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $response = curl_exec($ch);
            curl_close($ch);

            $xml = simplexml_load_string($response);
            $json = json_encode($xml);
            $data = json_decode($json, true);

            if (array_key_exists("message", $data)) {
                throw new \Exception("Card could not be found on TCGPlayer.");
            }

            return [
                "low"   => floatval($data['product']['lowprice']),
                "mid"   => floatval($data['product']['avgprice']),
                "high"  => floatval($data['product']['hiprice']),
                "link"  => $data['product']['link']
            ];
        }
    }
}