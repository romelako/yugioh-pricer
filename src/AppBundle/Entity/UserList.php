<?php

namespace AppBundle\Entity;

/**
 * UserList
 */
class UserList
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $listName;

    /**
     * @var string
     */
    private $listContent;

    /**
     * @var int
     */
    private $multiplier;

    /**
     * @var string
     */
    private $pricing;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var string
     */
    private $identifier;

    /**
     * @var User
     */
    private $user;

    public function __construct()
    {
        $this->created = new \DateTime;
        $this->calculateIdentifier();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set listName
     *
     * @param string $listName
     *
     * @return UserList
     */
    public function setListName($listName)
    {
        $this->listName = $listName;

        return $this;
    }

    /**
     * Get listName
     *
     * @return string
     */
    public function getListName()
    {
        return $this->listName;
    }

    /**
     * Set listContent
     *
     * @param string $listContent
     *
     * @return UserList
     */
    public function setListContent($listContent)
    {
        $this->listContent = $listContent;

        return $this;
    }

    /**
     * Get listContent
     *
     * @return string
     */
    public function getListContent()
    {
        return $this->listContent;
    }

    /**
     * @return int
     */
    public function getMultiplier()
    {
        return $this->multiplier;
    }

    /**
     * @param int $multiplier
     * @return $this
     */
    public function setMultiplier($multiplier)
    {
        $this->multiplier = $multiplier;
        return $this;
    }

    /**
     * @return string
     */
    public function getPricing()
    {
        return $this->pricing;
    }

    /**
     * @param string $pricing
     * @return $this
     */
    public function setPricing($pricing)
    {
        $this->pricing = $pricing;
        return $this;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return UserList
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     * @return $this
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    public function serialize()
    {
        return [
            "list"          => json_decode($this->listContent, true),
            "updated"       => $this->created->format("m/d/Y g:i a"),
            "name"          => $this->listName,
            "identifier"    => $this->identifier,
            "pricing"       => $this->pricing,
            "multiplier"    => $this->multiplier
        ];
    }

    public function calculateIdentifier()
    {
        $this->identifier = $this->generateRandomString();
    }

    private function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}

