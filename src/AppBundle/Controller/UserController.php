<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Doctrine\DBAL\DBALException;

class UserController extends Controller
{
    /**
     * @Route("/login", name="user_login")
     * @param Request $request
     * @return JsonResponse
     */
    public function loginAction(Request $request)
    {
        $username = $request->request->get("username");
        $password = $request->request->get("password");
        $remember = $request->request->get("remember");
        $content = [
            "success"   => false,
            "message"   => ""
        ];
        $response = new JsonResponse();

        try {
            $user = $this->getDoctrine()
                ->getRepository("AppBundle:User")
                ->findOneByUsername($username);

            if (null == $user)
                throw new \Exception("Username does not exist.");

            if (!password_verify($password, $user->getPassword()))
                throw new \Exception("Username/password combination is invalid.");

            $this->loginUser($user, $request);

            if ($remember == "true") {
                $this->get('app.remember_me_service')->createRememberMeCookie(
                    $user,
                    $response
                );
            }

            $content['success'] = true;
        } catch(\Exception $e) {
            $content['message'] = $e->getMessage();
        }

        $response->setContent(json_encode($content));
        return $response;
    }

    /**
     * @Route("/logout", name="logout")
     * @param Request $request
     * @return mixed
     */
    public function logout(Request $request)
    {
        $this->get("security.token_storage")->setToken(null);
        $request->getSession()->invalidate();
        $response = new RedirectResponse($this->generateUrl("homepage"));
        $response->headers->clearCookie("remember_me");

        return $response;
    }

    /**
     * @Route("/register", name="user_registration")
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request)
    {
        $username = $request->request->get("username");
        $password = $request->request->get("password");
        $email    = $request->request->get("email");
        $remember = $request->request->get("remember");

        $content = [
            "success"   => false,
            "message"   => ""
        ];
        $response = new JsonResponse();

        $user = new User();
        $user->setUsername($username)
            ->setEmail($email);

        $encodedPassword = $this->get("security.password_encoder")
            ->encodePassword($user, $password);

        $user->setPassword($encodedPassword);

        $em = $this->getDoctrine()->getManager();

        try {
            $em->persist($user);
            $em->flush();

            $this->loginUser($user, $request);

            if ($remember == "true") {
                $this->get('app.remember_me_service')->createRememberMeCookie(
                    $user,
                    $response
                );
            }

            $content['success'] = true;
        } catch (DBALException $e) {
            $content['message'] = "That username or email already exists.";
        }

        $response->setContent(json_encode($content));
        return $response;
    }

    private function loginUser($user, $request)
    {
        $token = new UsernamePasswordToken($user, $user->getPassword(), "main", $user->getRoles());
        $this->get("security.token_storage")->setToken($token);

        $event = new InteractiveLoginEvent($request, $token);
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
    }
}

