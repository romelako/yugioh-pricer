<?php

namespace AppBundle\Controller;

use AppBundle\Entity\UserList;
use PriceGrabber\Card;
use PriceGrabber\YugiohPricesPriceGrabber;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use Symfony\Component\HttpFoundation\Request;
use TcgPlayerApi\TcgPlayerApi;
use Yugioh\CardFactory;

class ApiController extends Controller
{
    /**
     * Possible values: low|average|high
     *
     * @var string
     */
    const PRICE_POINT = "average";

    /**
     * @Get("/get_card_price", name="api_get_card_price")
     * @return JsonResponse
     */
    public function getCardPriceAction(Request $request)
    {
        $response = [
            "success" => false,
            "message" => "",
            "data" => null
        ];

        try {
            $card = CardFactory::create(
                $request->get("card"),
                $request->get("set"),
                $request->get("rarity")
            );

            $tcgApi = new TcgPlayerApi();
            $prices = $tcgApi->getCardPrices($card);

            $card->setPrices($prices);

            $response['success'] = true;
            $response['data'] = $card->serialize();
        } catch (\Exception $e) {
            $response['message'] = $e->getMessage();
        }

        return new JsonResponse($response);
    }

    /**
     * @Get("/get_all_cards", name="api_get_all_cards")
     * @return JsonResponse
     */
    public function getAllCardsAction()
    {
        $memcache = new \Memcache;
        $memcache->connect('localhost', 11211);
        $json = $memcache->get('YGO_CARDLIST');

        if (!$json) {
            $path = $this->get("kernel")->getRootDir() . "/../cardlist.json";
            $json = file_get_contents($path);
            $json = json_decode($json, true);
            $memcache->set('YGO_CARDLIST', $json, false, 60 * 60 * 24);
        }

        return new JsonResponse($json);
    }

    /**
     * @Get("/get_card_sets_rarities", name="api_get_card_sets_rarities")
     * @return JsonResponse
     */
    public function getCardSetsAction(Request $request)
    {
        $ch = curl_init();
        $url = "http://yugiohprices.com/api/card_versions/" . urlencode($request->get('card'));

        curl_setopt_array($ch, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1,
        ]);

        $response = json_decode(curl_exec($ch), true);
        curl_close($ch);

        if ($response['status'] == "success") {
            $sets = [];
            $rarities = [];
            foreach ($response['data'] as $d) {
                $set = explode("-", $d['print_tag'])[0];
                if (!in_array($set, $sets))
                    $sets[] = $set;

                if (!in_array($d['rarity'], $rarities))
                    $rarities[] = $d['rarity'];
            }
            return new JsonResponse([
                "sets"      => $sets,
                "rarities"  => $rarities
            ]);
        }

        return new JsonResponse(["sets" => [], "rarities" => []]);
    }

    /**
     * @Get("/get_session_data", name="api_get_session_data")
     * @param Request $request
     * @return JsonResponse
     */
    public function getSessionDataAction(Request $request)
    {
        $list = $request->getSession()->get("list");

        // Change quantities/prices into integers and floats respectively
        if ($list) {
            $list['multiplier'] = intval($list['multiplier']);

            foreach ($list['cards'] as &$l) {
                $l['prices']['low'] = floatval($l['prices']['low']);
                $l['prices']['mid'] = floatval($l['prices']['mid']);
                $l['prices']['high'] = floatval($l['prices']['high']);
                $l['quantity'] = intval($l['quantity']);
            }
        } else {
            $list['cards'] = [];
        }

        if ($this->getUser() != null) {
            $list['loggedIn'] = true;
            $userLists = $this->getDoctrine()
                ->getRepository("AppBundle:UserList")
                ->findByUser($this->getUser());

            if (null == $userLists) {
                $list['userLists'] = [];
            } else {
                foreach ($userLists as $ul) {
                    $list['userLists'][] = [
                        "id" => $ul->getId(),
                        "listName" => $ul->getListName()
                    ];
                }
            }
        } else {
            $list['loggedIn'] = false;
            $list['userLists'] = [];
        }

        return new JsonResponse($list);
    }

    /**
     * @Post("/update_list", name="api_update_list")
     * @param Request $request
     * @return JsonResponse
     */
    public function updateListAction(Request $request)
    {
        $cards = $request->request->get("cards");
        $pricing = $request->request->get("pricing");
        $multiplier = $request->request->get("multiplier");

        $request->getSession()
            ->set("list", [
                "cards" => $cards != null ? $cards : [],
                "pricing" => $pricing,
                "multiplier" => $multiplier,
                "userLists" => []
            ]);
        return new JsonResponse("success");
    }

    /**
     * @Post("/save_list", name="api_save_list")
     * @param Request $request
     * @return JsonResponse
     */
    public function saveListAction(Request $request)
    {
        $cards = $request->request->get("cards");
        $name = $request->request->get('name');
        $multiplier = $request->request->get("multiplier");
        $pricing = $request->request->get('pricing');
        $em = $this->getDoctrine()->getManager();

        // Check if list with name already exists to overwrite
        /* @var UserList $userlist */
        $userList = $em->getRepository("AppBundle:UserList")
            ->findOneBy([
                "user" => $this->getUser(),
                "listName" => $name
            ]);

        if (null == $userList) {
            $userList = new UserList();
            $userList->setListContent(json_encode($cards))
                ->setListName($name)
                ->setUser($this->getUser())
                ->setMultiplier($multiplier)
                ->setPricing($pricing);
            $em->persist($userList);
        } else {
            $userList->setListContent(json_encode($cards))
                ->setCreated(new \DateTime())
                ->setPricing($pricing)
                ->setMultiplier($multiplier);
        }

        // In case random string is duplicated
        try {
            $em->flush();
        } catch (DBALException $e) {
            $userList->calculateIdentifier();
            $em->flush();
        }

        return new JsonResponse($userList->getIdentifier());
    }

    /**
     * @Post("/delete_list", name="api_delete_list")
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteListAction(Request $request)
    {
        $id = intval($request->request->get("id"));
        $em = $this->getDoctrine()->getManager();
        $response = [
            "success"   => false,
            "message"   => ""
        ];

        // Check if list with name already exists to overwrite
        /* @var UserList $userlist */
        $userList = $em->getRepository("AppBundle:UserList")
            ->findOneBy([
                "user" => $this->getUser(),
                "id" => $id
            ]);

        if (null == $userList) {
            $response['message'] = "User list with ID#{$id} could not be found.";
        } else {
            $em->remove($userList);
            $em->flush();
            $response['success'] = true;
        }

        return new JsonResponse($response);
    }

    /**
     * @Get("/view_list", name="api_view_list")
     * @param Request $request
     * @return JsonResponse
     */
    public function viewListAction(Request $request)
    {
        $id = intval($request->query->get('id'));
        $userList = $this->getDoctrine()
            ->getRepository("AppBundle:UserList")
            ->findOneBy([
                "user" => $this->getUser(),
                "id" => $id
            ]);

        $response = [
            "success" => false,
            "message" => "",
            "list" => null
        ];

        if (null == $userList) {
            $response['message'] = "Could not find list w/ ID#{$id} for user {$this->getUser()->getUsername()}";
        } else {
            $response['success'] = true;
            $response['list'] = $userList->serialize();
        }

        return new JsonResponse($response);
    }

    /**
     * @Get("/view_list_by_identifier", name="api_view_list_by_identifier")
     * @param Request $request
     * @return JsonResponse
     */
    public function viewListByIdentifierAction(Request $request)
    {
        $identifier = $request->query->get("identifier");
        $userList = $this->getDoctrine()
            ->getRepository("AppBundle:UserList")
            ->findOneByIdentifier($identifier);

        $response = [
            "success" => false,
            "message" => "",
            "list" => null
        ];

        if (null == $userList) {
            $response['message'] = "Could not find list w/ identifier: {$identifier}.";
        } else {
            $response['success'] = true;
            $response['list'] = $userList->serialize();
        }

        return new JsonResponse($response);
    }

    /**
     * @Get("/get_user_list_metadata", name="api_get_user_list_metadata")
     * @param Request $request
     * @return JsonResponse
     */
    public function getUserListMetadataAction(Request $request)
    {
        $response = [
            "success" => false,
            "message" => "",
            "metadata" => []
        ];

        $userLists = $this->getDoctrine()
            ->getRepository("AppBundle:UserList")
            ->findByUser($this->getUser());

        $lists = [];

        if (null != $userLists) {
            foreach($userLists as $l) {
                $lists[] = [
                    "id"        => $l->getId(),
                    "listName"  => $l->getListName()
                ];
            }
            $response['metadata'] = $lists;
            $response['success'] = true;
        }

        return new JsonResponse($response);
    }
}

