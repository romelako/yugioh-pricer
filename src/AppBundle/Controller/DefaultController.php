<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Route("/{identifier}", name="homepage_with_identifier", requirements={"identifier" = "^(?!login|logout|register)[a-zA-Z0-9]{10}$"})
     */
    public function indexAction(Request $request)
    {
        if ($request->cookies->get("remember_me")) {
            $cookie = $request->cookies->get("remember_me");
            $token = $cookie;

            $authToken = $this->getDoctrine()
                ->getRepository("AppBundle:AuthToken")
                ->findOneByToken($token);

            if (null !== $authToken) {
                $today = new \DateTime();

                if ($today < $authToken->getExpires()) {
                    $token = new UsernamePasswordToken(
                        $authToken->getUser(),
                        $authToken->getUser()->getPassword(),
                        "main",
                        $authToken->getUser()->getRoles());
                    $this->get("security.token_storage")->setToken($token);

                    $event = new InteractiveLoginEvent($request, $token);
                    $this->get("event_dispatcher")
                        ->dispatch("security.interactive_login", $event);
                }
            }
        }

        // replace this example code with whatever you need
        return $this->render("index.html.twig");
    }
}
