<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use PHPHtmlParser\Dom;

class GenerateCardlistCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName("app:generate-cardlist")
            ->setDescription("Generates JSON list of all cards and stores it in the database.")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dom = new Dom;
        $dom->loadFromUrl("https://www.yugiohcardguide.com/card_list.html");
        $html = $dom->find('tr td a');

        var_dump($html);
    }
}