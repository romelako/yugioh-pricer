<?php

namespace AppBundle\Common;

use AppBundle\Entity\AuthToken;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Cookie;
use Doctrine\ORM\EntityManager;

class RememberMeCookieService
{
    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function createRememberMeCookie($user, JsonResponse $response)
    {
        $this->deleteOldToken($user);

        $token = bin2hex(random_bytes(20));
        $expires = new \DateTime();
        $expires->modify("+1 year");

        $authToken = new AuthToken();
        $authToken->setUser($user)
            ->setToken($token)
            ->setExpires($expires);

        $this->em->persist($authToken);
        $this->em->flush();

        $cookie = new Cookie("remember_me", $token, $expires);
        $response->headers->setCookie($cookie);
    }

    private function deleteOldToken($user)
    {
        $authToken = $this->em->getRepository("AppBundle:AuthToken")
            ->findOneByUser($user);

        if (null != $authToken)
            $this->em->remove($authToken);
    }
}