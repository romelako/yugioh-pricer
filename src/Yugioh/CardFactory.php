<?php

namespace Yugioh;

use YugiohPricesApi\YugiohPricesApi;
use YugiohPricesApi\YugiohPricesCardValidator;

class CardFactory
{
    public static function create($name, $set, $rarity)
    {
        $ypApi = new YugiohPricesApi();

        // Use price data to normalize the set and rarity
        $priceData = $ypApi->getPriceData($name);

        YugiohPricesCardValidator::validatePriceResponse($priceData, $set, $rarity);

        $card = new Card();

        foreach($priceData['data'] as $data) {
            if (strpos(strtolower($data['print_tag']), strtolower($set)) !== false
                && strcasecmp($rarity, $data['rarity']) == 0) {

                // Exclude cards like LOB-E053 with weird set name
                if (preg_match('/[A-Z]{3}-[A-Z][\d]{3}/', $data['print_tag']))
                    continue;

                $card->setTag($data['print_tag'])
                    ->setRarity($data['rarity'])
                    ->setSet($data['name']);
            }
        }

        // Get the card information to normalize the name
        $cardData = $ypApi->getCardData($name);

        YugiohPricesCardValidator::validateInfoResponse($cardData);

        $card->setName($cardData['data']['name']);

        return $card;
    }
}