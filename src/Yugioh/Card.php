<?php

namespace Yugioh;

class Card
{
    private $name;
    private $set;
    private $tag;
    private $rarity;
    private $prices;
    private $quantity = 1;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param mixed $tag
     * @return $this
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSet()
    {
        return $this->set;
    }


    /**
     * @param mixed $set
     * @return $this
     */
    public function setSet($set)
    {
        $this->set = $set;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRarity()
    {
        return $this->rarity;
    }

    /**
     * @param mixed $rarity
     * @return $this
     */
    public function setRarity($rarity)
    {
        $this->rarity = $rarity;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrices()
    {
        return $this->prices;
    }

    /**
     * @param mixed $prices
     * @return $this
     */
    public function setPrices($prices)
    {
        $this->prices = $prices;
        return $this;
    }

    public function serialize()
    {
        return [
            "card"   => $this->name,
            "set"    => $this->getSet(),
            "rarity" => $this->rarity,
            "prices" => $this->prices,
            "quantity"  => $this->quantity,
            "image"  => $this->getImageLink(),
            "key"    => $this->getMemcachekey()
        ];
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return $this
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    public function getImageLink()
    {
        return "http://www.yugiohprices.com/api/card_image/".urlencode($this->name);
    }

    public function getMemcachekey()
    {
        return md5(strtolower($this->name.$this->set.$this->rarity));
    }
}