<?php

namespace YugiohPricesApi;

use Yugioh\Card;

class YugiohPricesApi
{
    const API_BASE = "http://yugiohprices.com/api/";
    const PRICES_ENDPOINT = "get_card_prices/";
    const INFO_ENDPOINT = "card_data/";

    private $memcache;

    public function __construct()
    {
        $this->memcache = new \Memcache;
        $this->memcache->connect('localhost', 11211);
    }

    public function getPriceData($cardName)
    {
        $memcacheKey = md5(strtolower($cardName))."_price";
        if ($this->memcache->get($memcacheKey)) {
            $data = $this->memcache->get($memcacheKey);
        } else {
            $url = self::API_BASE.self::PRICES_ENDPOINT
                .urlencode($cardName);
            $data = $this->getApiResponse($url);
            $this->memcache->set(
                $memcacheKey,
                $data,
                false,
                60 * 60 * 24    // Store for 24 hours
            );
        }

        return $data;
    }

    public function getCardData($cardName)
    {
        $memcacheKey = md5(strtolower($cardName))."_data";
        if ($this->memcache->get($memcacheKey)) {
            $data = $this->memcache->get($memcacheKey);
        } else {
            $url = self::API_BASE.self::INFO_ENDPOINT
                .urlencode($cardName);
            $data = $this->getApiResponse($url);
            $this->memcache->set(
                $memcacheKey,
                $data,
                false,
                60 * 60 * 24    // Store for 24 hours
            );
        }

        return $data;
    }

    private function getApiResponse($url)
    {
        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1
        ]);
        $response = curl_exec($ch);
        curl_close($ch);
        $response =  json_decode($response, true);

        if ($response)
            return $response;

        throw new \Exception("YugiohPrices API did not return valid JSON.");
    }
}