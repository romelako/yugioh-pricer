<?php

namespace YugiohPricesApi;

class YugiohPricesCardValidator
{
    public static function validatePriceResponse($response, $set, $rarity)
    {
        if ($response['status'] == 'fail')
            throw new \Exception($response['message']);

        foreach($response['data'] as $data)
        {
            if (strpos(strtolower($data['print_tag']), strtolower($set)) !== false
                && strcasecmp($rarity, $data['rarity']) == 0
                && $data['price_data']['status'] == "success") {
                    return true;
            }
        }

        throw new \Exception("Prices for that set/rarity combination don't exist.");
    }

    public static function validateInfoResponse($response)
    {
        if ($response['status'] == 'fail')
            throw new \Exception($response['message']);

        return true;
    }
}