import React from 'react';
import Lists from "./Lists.jsx";

class Settings extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            visible: "settings"
        };
    }

    render() {
        return (
            <div>
                <Tabs visible={this.state.visible} />

                <div className="tab-content">
                    <div className="tab-pane active" id="settings">
                        <div className="settings-content">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="col-md-12">
                                        <h4>Pricing Scheme:</h4>
                                        <div className="col-md-4">
                                            <input type="radio" name="pricing" onChange={() => this.props.changePricing("low")} checked={this.props.pricing == "low" && "checked"} /> Low
                                        </div>

                                        <div className="col-md-4">
                                            <input type="radio" name="pricing" onChange={() => this.props.changePricing("mid")} checked={this.props.pricing == "mid" && "checked"} /> Mid/Average
                                        </div>

                                        <div className="col-md-4">
                                            <input type="radio" name="pricing" onChange={() => this.props.changePricing("high")} checked={this.props.pricing == "high" && "checked"} /> High
                                        </div>
                                    </div>

                                    <div className="col-md-12">
                                        <h4 style={{ marginTop: "20px" }}>Multiplier:</h4>
                                        <div className="col-md-3">
                                            <div className="input-group">
                                                <input id="settings-multiplier" type="text" className="form-control" value={this.props.multiplier} onChange={(e) => this.props.updateMultiplier(e.target.value)} />
                                                <span className="input-group-addon" id="basic-addon2">%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="tab-pane" id="lists">
                        <Lists userLists={this.props.userLists}
                               loggedIn={this.props.loggedIn}
                               saveList={this.props.saveList}
                               viewList={this.props.viewList}
                               deleteList={this.props.deleteList} />
                    </div>
                    <div className="tab-pane" id="tips">
                        <div className="col-md-12">
                            <h4>Usage Tips:</h4>
                            <ul>
                                <li>Use tab (desktop) to change between fields</li>
                                <li>Use the arrow keys to choose list items</li>
                                <li>Set and rarity field will automatically populate with the card's possible sets/rarities</li>
                                <li>Use [Enter] to add the card to the list</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function Tabs(props) {
    return (
        <div>
            <ul className="nav nav-tabs">
                <li className={props.visible == "settings" ? "active" : ""}>
                    <a href="#settings" data-toggle="tab">
                        Settings
                    </a>
                </li>

                <li className={props.visible == "Lists" ? "active" : ""}>
                    <a href="#lists" data-toggle="tab">
                        Lists
                    </a>
                </li>

                <li className={props.visible == "Tips" ? "active" : ""}>
                    <a href="#tips" data-toggle="tab">
                        Tips
                    </a>
                </li>
            </ul>
        </div>
    )
}

$(function(){
    $(".tabs a").click(function(e){
        e.preventDefault();
        $(this).tab('show');
    });
});

module.exports = Settings;