import React from 'react';
import {render} from 'react-dom';
import CardInput from "./CardInput.jsx";
import Settings from "./Settings.jsx";

class App extends React.Component {
    constructor() {
        super();

        this.addCard = this.addCard.bind(this);
        this.addAlert = this.addAlert.bind(this);
        this.addQuantity = this.addQuantity.bind(this);
        this.removeQuantity = this.removeQuantity.bind(this);
        this.removeCard = this.removeCard.bind(this);
        this.updateSessionList = this.updateSessionList.bind(this);
        this.getSessionData = this.getSessionData.bind(this);
        this.clearList = this.clearList.bind(this);
        this.changePricing = this.changePricing.bind(this);
        this.updateMultiplier = this.updateMultiplier.bind(this);
        this.saveList = this.saveList.bind(this);
        this.deleteList = this.deleteList.bind(this);
        this.getUserLists = this.getUserLists.bind(this);
        this.viewList = this.viewList.bind(this);
        this.viewListByIdentifier = this.viewListByIdentifier.bind(this);

        this.state = {
            cards: [],
            errors: {
                messages: [],
                visible: false
            },
            pricing: "mid",
            multiplier: 100,
            loggedIn: false,
            userLists: []
        };

        var self = this;
        this.getSessionData().done(function(response){
            self.setState({
                cards: response.cards,
                pricing: response.pricing ? response.pricing : "mid",
                multiplier: response.multiplier ? response.multiplier : 100,
                userLists: response.userLists,
                loggedIn: response.loggedIn
            });

            let identifier = location.href.split("/").pop();
            if (identifier.length == 10)
                self.viewListByIdentifier(identifier);
        });
    }

    render () {
        return (
            <div>
                <div className="row">
                    <div className="col-md-6">
                        <ErrorAlert messages={this.state.errors.messages} visible={this.state.errors.visible} />
                        <CardInput addCard={this.addCard} addAlert={this.addAlert} clearList={this.clearList} />
                    </div>

                    <div className="col-md-6">
                        <Settings changePricing={this.changePricing}
                                  pricing={this.state.pricing}
                                  multiplier={this.state.multiplier}
                                  updateMultiplier={this.updateMultiplier}
                                  userLists={this.state.userLists}
                                  loggedIn={this.state.loggedIn}
                                  saveList={this.saveList}
                                  viewList={this.viewList}
                                  deleteList={this.deleteList} />
                    </div>
                </div>

                <hr />

                <div className="row">
                    <div className="col-md-12">
                        <Total cards={this.state.cards}
                               pricing={this.state.pricing}
                               multiplier={this.state.multiplier} />

                        <table className="table table-striped table-bordered hidden-sm hidden-xs">
                            <thead>
                            <tr>
                                <th style={{width: '10%'}}>Image</th>
                                <th>Card Name</th>
                                <th>Set Name</th>
                                <th>Rarity</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.state.cards.length == 0 &&
                                <tr>
                                    <td colSpan="7" style={{textAlign: "center"}}>
                                        There are no cards in the table
                                    </td>
                                </tr>
                            }
                            {this.state.cards.map((card) => (
                                <Row name={card.card}
                                     set={card.set}
                                     rarity={card.rarity}
                                     prices={card.prices}
                                     key={card.key}
                                     quantity={card.quantity}
                                     addQuantity={() => this.addQuantity(card.key)}
                                     removeQuantity={() => this.removeQuantity(card.key)}
                                     removeCard={() => this.removeCard(card.key)}
                                     image={card.image}
                                     pricing={this.state.pricing}
                                     multiplier={this.state.multiplier} />
                            ))}
                            </tbody>
                        </table>

                        <table className="table table-stripe table-bordered hidden-md hidden-lg hidden-xl">
                            <thead>
                                <tr>
                                    <th>Picture</th>
                                    <th>Information</th>
                                </tr>
                            </thead>

                            <tbody>
                                {this.state.cards.length == 0 &&
                                <tr>
                                    <td colSpan="7" style={{textAlign: "center"}}>
                                        There are no cards in the table
                                    </td>
                                </tr>
                                }
                                {this.state.cards.map((card) => (
                                    <SmallRow name={card.card}
                                         set={card.set}
                                         rarity={card.rarity}
                                         prices={card.prices}
                                         key={card.key}
                                         quantity={card.quantity}
                                         addQuantity={() => this.addQuantity(card.key)}
                                         removeQuantity={() => this.removeQuantity(card.key)}
                                         removeCard={() => this.removeCard(card.key)}
                                         image={card.image}
                                         pricing={this.state.pricing}
                                         multiplier={this.state.multiplier} />
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>

                <div className="pull-right" id="credits">
                    Prices are based on <a href="http://www.tcgplayer.com/">TCGPlayer</a> mid.
                    Card information provided by <a href="http://www.yugiohprices.com/">YugiohPrices</a>.
                </div>
            </div>
        );
    }

    addCard(card) {
        let cards = this.state.cards.slice();

        if (!App.inArray(card, cards)) {
            cards.push(card);
            this.setState({cards: cards});
            this.updateSessionList();
        } else {
            this.addAlert({
                messages: ["This card already exists on the list."],
                visible: true
            })
        }
    }

    addAlert(errors) {
        this.setState({
            errors: errors
        });
    }

    addQuantity(key) {
        let cards = this.state.cards.slice();

        for(let i = 0; i < cards.length; i++) {
            if (cards[i].key == key) {
                cards[i].quantity++;
            }
        }

        this.setState({
            cards: cards
        }, () => this.updateSessionList());
    }

    removeQuantity(key) {
        let cards = this.state.cards.slice();

        for(let i = 0; i < cards.length; i++) {
            if (cards[i].key == key && cards[i].quantity != 0) {
                cards[i].quantity--;
            }
        }

        this.setState({
            cards: cards
        }, () => this.updateSessionList());
    }

    removeCard(key) {
        let cards = this.state.cards.slice();
        let newCards = [];

        for(let i = 0; i < cards.length; i++) {
            if (cards[i].key != key)
                newCards.push(cards[i]);
        }

        this.setState({
            cards: newCards
        }, () => this.updateSessionList());
    }

    getSessionData()
    {
        return $.ajax({
            type: "GET",
            url: "/api/get_session_data"
        });
    }

    updateSessionList()
    {
        let cards = this.state.cards;
        $.ajax({
            type: "POST",
            url: "/api/update_list",
            data: {
                cards: cards,
                pricing: this.state.pricing,
                multiplier: this.state.multiplier
            }
        });
    }

    clearList()
    {
        this.setState({
            cards: []
        }, () => this.updateSessionList())
    }

    static inArray(needle, haystack) {
        for (let i = 0; i < haystack.length; i++) {
            if (haystack[i].key == needle.key)
                return true;
        }
        return false;
    }

    changePricing(pricing) {
        this.setState({
            pricing: pricing
        }, () => this.updateSessionList());
    }

    updateMultiplier(multiplier) {
        var check = /^[0-9]+$/.test(multiplier);
        if (check == true) {
            this.setState({
                multiplier: multiplier
            }, () => this.updateSessionList())
        }
    }

    saveList(name) {
        let self = this;
        $.ajax({
            url: "/api/save_list",
            method: "POST",
            data: {
                cards: this.state.cards,
                multiplier: this.state.multiplier,
                pricing: this.state.pricing,
                name: name
            }
        }).done(function(response){
            let shareLink = "http://www.yugiohpricer.com/"+response;
            $("#share-link").val(shareLink);
            self.getUserLists();
        });
    }

    deleteList(id) {
        let self = this;
        $.ajax({
            url: "/api/delete_list",
            method: "POST",
            data: {
                id: id
            }
        }).done(function(response){
            if (response.success) {
                console.log("Deletion successful");
                self.getUserLists();
            } else {
                console.log(response.message);
            }
        });
    }

    viewList(id) {
        let self = this;
        $.ajax({
            url: "/api/view_list",
            method: "GET",
            data: {
                id: id
            }
        }).done(function(response){
            if (response.success) {
                self.setState({
                    cards: response.list.list != null ? response.list.list : [],
                    currentList: response.list,
                    multiplier: response.list.multiplier,
                    pricing: response.list.pricing
                });

                let shareLink = "http://www.yugiohpricer.com/"+response.list.identifier;;
                $("#share-link").val(shareLink);
            } else {
                console.log(response.message);
            }
        });
    }

    viewListByIdentifier(identifier) {
        let self = this;
        $.ajax({
            url: "/api/view_list_by_identifier",
            method: "GET",
            data: {
                identifier: identifier
            }
        }).done(function(response){
            if (response.success) {
                self.setState({
                    cards: response.list.list != null ? response.list.list : [],
                    currentList: response.list,
                    multiplier: response.list.multiplier,
                    pricing: response.list.pricing
                });
            } else {
                console.log(response.message);
            }
        });
    }

    getUserLists() {
        if (this.state.loggedIn) {
            let self = this;
            $.ajax({
                url: "/api/get_user_list_metadata",
                method: "GET"
            }).done(function(response){
                if (response.success) {
                    self.setState({
                        userLists: response.metadata
                    });
                } else {
                    console.log(response.message)
                }
            });
        }
    }
}

class Row extends React.Component {
    constructor(props) {
        super(props);

        this.renderPrice = this.renderPrice.bind(this);
    }

    renderPrice(quantity) {
        let price;

        if (this.props.pricing == "low")
            price = this.props.prices.low;
        else if (this.props.pricing == "mid")
            price = this.props.prices.mid;
        else
            price = this.props.prices.high;

        price = price * this.props.multiplier / 100;
        price = price.toFixed(2);
        let quantityPrice = (price * quantity).toFixed(2);

        if (quantity > 1) {
            return (
                <span>
                    ${price} [x{quantity}: ${quantityPrice}]
                </span>
            )
        } else {
            return (
                <span>
                    ${price}
                </span>
            )
        }
    }

    render() {
        return (
            <tr>
                <td><img className="responsive" style={{width: '50px'}} src={this.props.image} /></td>
                <td>
                    <a href={this.props.prices.link}>
                        {this.props.name}
                    </a>
                </td>
                <td>{this.props.set}</td>
                <td>{this.props.rarity}</td>
                <td>{this.renderPrice(this.props.quantity)}</td>
                <td>
                    {this.props.quantity}
                    <div className="btn-group btn-group-sm" role="group" style={{display: 'inline-block', marginLeft: '15px'}}>
                        <button type="button" className="btn btn-default" onClick={this.props.addQuantity}>+</button>
                        <button type="button" className="btn btn-default" onClick={this.props.removeQuantity}>-</button>
                    </div>
                </td>
                <td>
                    <button type="button" className="btn btn-danger" onClick={this.props.removeCard}>
                        <i className="fa fa-times" />
                    </button>
                </td>
            </tr>
        )
    }
}

class SmallRow extends React.Component {
    constructor(props) {
        super(props);
        this.renderPrice = this.renderPrice.bind(this);
    }

    renderPrice(quantity) {
        let price;

        if (this.props.pricing == "low")
            price = this.props.prices.low;
        else if (this.props.pricing == "mid")
            price = this.props.prices.mid;
        else
            price = this.props.prices.high;

        price = price * this.props.multiplier / 100;
        price = price.toFixed(2);
        let quantityPrice = (price * quantity).toFixed(2);

        if (quantity > 1) {
            return (
                <span>
                    ${price} [x{quantity}: ${quantityPrice}]
                </span>
            )
        } else {
            return (
                <span>
                    ${price}
                </span>
            )
        }
    }

    render() {
        return (
            <tr>
                <td><img className="responsive" style={{width: '100px'}} src={this.props.image} /></td>
                <td>
                    <a href={this.props.prices.link}>
                        {this.props.name}
                    </a><br />
                    {this.props.set}<br />
                    {this.props.rarity}<br />
                    {this.renderPrice(this.props.quantity)}<br />
                    <hr />
                    <b>Quantity:</b> {this.props.quantity}
                    <div className="btn-group btn-group-sm" role="group" style={{display: 'inline-block', marginLeft: '15px'}}>
                        <button type="button" className="btn btn-default" onClick={this.props.addQuantity}>+</button>
                        <button type="button" className="btn btn-default" onClick={this.props.removeQuantity}>-</button>
                    </div><br />
                    <hr />
                    <button type="button" className="btn btn-danger" onClick={this.props.removeCard}>
                        Remove
                    </button>
                </td>
            </tr>
        )
    }
}

function ErrorAlert(props) {
    if (!props.visible || props.messages == null) {
        return null;
    }

    return (
        <div className="alert alert-danger" id="card-input-error">
            <ul>
                {props.messages.map((error) => (
                    <li>{error}</li>
                ))}
            </ul>
        </div>
    );
}

function Total(props) {
    let total = 0;

    for(let i = 0; i < props.cards.length; i++) {
        let price;

        if (props.pricing == "low")
            price = props.cards[i].prices.low;
        else if (props.pricing == "mid")
            price = props.cards[i].prices.mid;
        else
            price = props.cards[i].prices.high;

        price = price * props.multiplier / 100;
        price = price.toFixed(2);

        total += price * props.cards[i].quantity;
    }

    return (
        <span>
            <h2>
                <b>Total:</b> ${total.toFixed(2)}
            </h2>
        </span>
    )
}

render(<App/>, document.getElementById('app'));