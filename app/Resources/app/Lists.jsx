import React from 'react';

class Lists extends React.Component {
    constructor(props) {
        super(props);

        this.saveList = this.saveList.bind(this);
        this.viewList = this.viewList.bind(this);
        this.deleteList = this.deleteList.bind(this);
    }

    saveList() {
        let name = $("#list-name").val().trim();
        this.props.saveList(name);
    }

    updateListName() {
        let listName = $("#list-selection").find("option:selected").text();
        $("#list-name").val(listName);
    }

    viewList() {
        let id = $("#list-selection").val();
        this.props.viewList(id);
    }

    deleteList() {
        let id = $("#list-selection").val();
        this.props.deleteList(id);
    }

    render() {
        if (!this.props.loggedIn)
            return (
                <div className="list-section">
                    You must be logged in to select and save your own custom lists.
                </div>
            );

        return(
            <div className="list-section">
                <div className="row">
                    <div className="col-md-6">
                        <div className="form-group">
                            <select className="form-control" defaultValue="" id="list-selection" onChange={this.updateListName}>
                                <option value="" disabled>--- Pick a list ---</option>
                                {this.props.userLists.map((list) => {
                                    return (<option key={list.id} value={list.id}>{list.listName}</option>);
                                })}
                            </select>
                        </div>
                    </div>

                    <div className="col-md-6">
                        <div className="form-group">
                            <div className="btn-group" role="group">
                                <button type="button" className="btn btn-primary" onClick={this.viewList}>View</button>
                                <button type="button" className="btn btn-info" onClick={this.saveList}>Save</button>
                                <button type="button" className="btn btn-danger" onClick={this.deleteList}>Delete</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-12">
                        <div className="form-group">
                            <label>List Name</label>
                            <input type="text" className="form-control" id="list-name" />
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-12">
                        <div className="form-group">
                            <label>Share</label>
                            <div className="input-group">
                                <input type="text" readOnly className="form-control" id="share-link" />
                                <span className="input-group-addon copy" id="basic-addon2" data-clipboard-target="#share-link" data-original-title="" title="" style={{ cursor: "pointer", backgroundColor: "#FFF" }}>
                                    <i className="fa fa-clipboard" />
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

module.exports = Lists;