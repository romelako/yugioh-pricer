class CardPricer {
    priceCard(card) {
        return $.ajax({
            url: "/api/get_card_price",
            data: card
        });
    }
}

module.exports = CardPricer;