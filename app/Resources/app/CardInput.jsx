import React from 'react';
import CardPricer from "./CardPricer.jsx";

var pricer = new CardPricer;

class CardInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            errors: {
                messages: [],
                visible: false
            },
            loading: false
        };

        this.handleClick = this.handleClick.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
    }

    render() {
        return (
            <div>
                <LoadingOverlay visible={this.state.loading} />
                <div className="form-group">
                    <label>Card Name</label>
                    <input type="text" className="form-control" id="card" onKeyPress={this.handleKeyPress} />
                </div>

                <div className="form-group">
                    <label>Set Name</label>
                    <input type="text" className="form-control" id="set" onKeyPress={this.handleKeyPress} />
                </div>

                <div className="form-group">
                    <label>Rarity</label>
                    <input type="text" className="form-control" id="rarity" onKeyPress={this.handleKeyPress} />
                </div>

                <div className="form-group">
                    <div className="btn-group" role="group">
                        <button type="button" className="btn btn-primary" onClick={this.handleClick}>Add</button>
                        <button type="button" className="btn btn-danger" onClick={this.clearInputs}>Clear Input</button>
                        <button type="button" className="btn btn-warning" onClick={this.props.clearList}>Clear List</button>
                    </div>
                </div>
            </div>
        )
    }

    handleClick(e) {
        var errorMessages = this.validateInputs();

        if (errorMessages.length > 0) {
            this.props.addAlert({
                messages: errorMessages,
                visible: true
            })
        } else {
            this.props.addAlert({visible: true});

            var card = {
                card: $("#card").val(),
                set: $("#set").val(),
                rarity: $("#rarity").val()
            };

            this.setState({loading: true});
            var self = this;
            pricer.priceCard(card)
                .done(function(response){
                    self.setState({loading: false});
                    if (response.success) {
                        self.props.addAlert({visible: true});
                        self.props.addCard(response.data);
                        self.clearInputs();
                    } else {
                        self.props.addAlert({
                            messages: [response.message],
                            visible: true
                        });
                    }
                });
        }
    }

    validateInputs() {
        var errors = [];

        if ($("#card").val().trim() == "")
            errors.push("You must specify a card name");

        if ($("#set").val().trim() == "")
            errors.push("You must specify a set name");

        if ($("#rarity").val().trim() == "")
            errors.push("You must specify a rarity");

        return errors;
    }

    clearInputs() {
        $("#card").val("");
        $("#set").val("");
        $("#rarity").val("");
        $("#card").focus();
    }

    handleKeyPress(e) {
        if (e.key == "Enter") {
            this.handleClick();
        }
    }
}

function LoadingOverlay(props) {
    if (!props.visible) {
        return null;
    }

    return (
        <div id="loading-overlay"></div>
    )
}


module.exports = CardInput;